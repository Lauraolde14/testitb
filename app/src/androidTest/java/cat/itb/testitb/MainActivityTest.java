package cat.itb.testitb;

import android.util.Log;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions;
import com.schibsted.spain.barista.rule.BaristaRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityActivityScenarioRule
            = new ActivityScenarioRule<>(MainActivity.class);

    //Test function that checks title and button are displayed correctly.
    @Test
    public void check_elements_displayed_correctly_on_mainActivity() {
        onView(withId(R.id.textView_main)).check(matches(isDisplayed()));
        onView(withId(R.id.button_next)).check(matches(isDisplayed()));
    }

    //Test function that checks title name and button name.
    @Test
    public void check_item_text_and_button_is_correct() {
        onView(withId(R.id.textView_main)).check(matches(withText(R.string.main)));
        onView(withId(R.id.button_next)).check(matches(withText(R.string.next)));
    }

    //Test function that checks next button is clickable and when you press text change.
    @Test
    public void check_button_clickable_and_text_is_correct() {
        onView(withId(R.id.button_next)).check(matches(isClickable()));
        onView(withId(R.id.button_next)).perform(click()).check(matches(withText(R.string.back)));
    }

    //global strings
    static String USER_TO_BE_TYPED = "Laura";
    static String PASS_TO_BE_TYPED  = "1234";

    //Inserts value of global string user on username and closes keyboard.
    //Insers value of global string pass on password and closes keyboard.
    //Perform a click action on the button and make sure that it's change to "logged"
    //Fix the app (in mainActivity change the setText of the button)
    @Test
    public void login_from_behaviour() {
        onView(withId(R.id.username)).perform(typeText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.button_next)).perform(click()).check(matches(withText(R.string.logg)));
    }

    //Test function that checks when next button is clicked the activity changes to secondActivity.
    @Test
    public void check_change_activity_when_button_clicked() {
        onView(withId(R.id.button_next)).check(matches(isClickable())).perform(click());
        onView(withId(R.id.layoutActivity2)).check(matches(isDisplayed()));
    }

    //check navigation
    @Test
    public void check_change_activities_when_buttons_clicked() {
        onView(withId(R.id.button_next)).check(matches(isClickable())).perform(click());
        onView(withId(R.id.layoutActivity2)).check(matches(isDisplayed()));
        onView(withId(R.id.button_next2)).check(matches(isClickable())).perform(click());
        onView(withId(R.id.layoutActivity1)).check(matches(isDisplayed()));
    }

    @Rule
    public ActivityScenarioRule<MainActivity2> activityActivityScenarioRule2
            = new ActivityScenarioRule<>(MainActivity2.class);

    //Large test function
    @Test
    public void large_test_function() {
        onView(withId(R.id.username)).perform(typeText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.button_next)).check(matches(isClickable())).perform(click());
        onView(withId(R.id.layoutActivity2)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_main2)).check(matches(withText("Welcome back " + USER_TO_BE_TYPED)));
        onView(withId(R.id.button_next2)).check(matches(isClickable())).perform(click());
        onView(withId(R.id.layoutActivity1)).check(matches(isDisplayed()));
        onView(withId(R.id.username)).check(matches(withText("")));
        onView(withId(R.id.password)).check(matches(withText("")));
    }

    //Barista exemple

    @Rule
    public BaristaRule<MainActivity> baristaRule = BaristaRule.create(MainActivity.class);

    TestName name = new TestName();

    @Before
    public void setUp(){
        Log.i("Info","[START] - Launch Test: " + name.getMethodName());
        baristaRule.launchActivity();
    }

    @After
    public void tearDown(){
        Log.i("Info", "[FINISH] - Test: " + name.getMethodName());
    }

    @Test
    public void testTextoIsDisplayed(){
        BaristaVisibilityAssertions.assertDisplayed(R.id.button_next);
        Log.i("Info", "The element text is displayed.");
    }

}
