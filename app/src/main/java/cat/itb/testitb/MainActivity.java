package cat.itb.testitb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button button;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button_next);
        editText = findViewById(R.id.username);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //button.setText(R.string.back);
                button.setText(R.string.logg);
                Intent i = new Intent(MainActivity.this, MainActivity2.class);
                i.putExtra("user", editText.getText().toString());
                startActivity(i);
            }
        });
    }

}